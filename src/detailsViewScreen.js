import React, {Component} from 'react';
import { 
    Text, 
    View,
    Image,
} from 'react-native';

export default class DetailsViewScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      img_url: this.props.navigation.getParam('img_url', null), 
      price_formatted: this.props.navigation.getParam('price_formatted', null), 
      title: this.props.navigation.getParam('title', null), 
      summary: this.props.navigation.getParam('summary', null), 
      bedroom_number: this.props.navigation.getParam('bedroom_number', null), 
      bathroom_number: this.props.navigation.getParam('bathroom_number', null),
    }
  }
  render() {
    return (
        <View style={{flex: 1,}}>

            {/* code for the body part of the screen */}
            <View style={{flex: 1, }}> 
                <Image source={{uri: this.state.img_url}} style={{width: '100%', height: 300, }}/>
                <View style={{borderBottomColor: '#000000', borderBottomWidth: 1, }}>
                  <Text style={{fontSize: 20, padding: 10, fontWeight: 'bold', color: 'rgb(0, 138, 213)'}}>{this.state.price_formatted}</Text>
                  <Text style={{fontSize: 20, padding: 10, }}>{this.state.title}</Text>
                </View>
                <View>
                  <Text style={{paddingLeft: 10, fontSize: 15, paddingTop: 10, }}>{this.state.bedroom_number} bed flat{this.state.bathroom_number === "" ? " " : ", "+this.state.bathroom_number+" bathrooms"}</Text>
                  <Text style={{paddingLeft: 10, fontSize: 15, paddingTop: 10, }}>{this.state.summary}</Text>
                </View>
            </View>
        </View>
    );
  }
}

