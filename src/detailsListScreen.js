import React, {Component} from 'react';
import { 
    Text, 
    View, 
    Image, 
    TouchableOpacity,
    ScrollView,
    RefreshControl
} from 'react-native';

import * as axios from 'axios';

export default class DetailsListScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            url1: this.props.navigation.getParam('url1', null),
            url2: this.props.navigation.getParam('url2', null),
            page: this.props.navigation.getParam('page', null),
            responseData: this.props.navigation.getParam('responseData', null),
            refreshing: false,
        }
    }

    componentDidMount(){
        // console.warn(this.state.url1+this.state.page+this.state.url2+this.state.responseData.request.location);
    }

    _onRefresh = () => {
        this.setState({
            refreshing: true,
            page: this.state.page + 1,
        }, ()=> {
            axios.get(this.state.url1+this.state.page+this.state.url2+this.state.responseData.request.location)
            .then((response) => {
                this.setState({
                    refreshing: false,
                    responseData: response.data,
                });
            })
            .catch((error) => {
                this.setState({
                    refreshing: false,
                }, () => {
                    this.generateAlert(error.message);
                })
                return false;

            })
        });
    }

  render() {
    return (
        <View style={{flex: 1,}}>

            {/* code for the body */}
            <ScrollView 
            style={{flex: 1,}} 
            showsVerticalScrollIndicator={false}
            refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
                {/* code for the card part of the screen */}

                {
                    this.state.responseData.response.listings.map((item, index) => {
                        return(
                            <TouchableOpacity key={index} style={{width: '100%', height: 100, backgroundColor: '#ecf0f1', borderBottomColor: '#dddddd', borderBottomWidth: 1, flexDirection: 'row', }} onPress={() => this.props.navigation.navigate('DetailsViewScreen', {img_url: item.img_url, price_formatted: item.price_formatted, title: item.title, summary: item.summary, bedroom_number: item.bedroom_number, bathroom_number: item.bathroom_number})}>
                                <View style={{width: '30%', height: 100, justifyContent: 'center',}}>
                                    <Image source={{uri: item.img_url}} style={{width: 80, height: 80, marginLeft: 20, }}/>
                                </View>
                                <View style={{width: '70%', height: 100, justifyContent: 'center',}}>
                                    <Text style={{fontSize: 20, color: 'rgb(0, 131, 210)'}}>{item.price_formatted}</Text>
                                    <Text>{item.title}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })
                }

            </ScrollView>
        </View>
    );
  }
}

