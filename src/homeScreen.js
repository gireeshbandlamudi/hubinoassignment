import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    Alert,
} from 'react-native';

import * as axios from 'axios';

export default class HomeScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            locationName: "",
            isLoading: false,
        }
    }

    generateAlert = (text) => {
        Alert.alert(
            'Alert',
            text,
            [
              {
                text: 'Cancel',
                style: 'cancel',
              },
              {text: 'OK'},
            ],
            {cancelable: false},
          );
    }

    callRestApi = () => {
        if(this.state.locationName === ""){
            this.generateAlert("Location Name should not be empty");
            return;
        } else{
            var url1 = 'https://api.nestoria.co.uk/api?country=uk&pretty=1&encoding=json&listing_type=buy&action=search_listings&page=';
            var url2 = '&place_name=';
            var page = 1;
            var fullURL = url1+page+url2+this.state.locationName;

            this.setState({
                isLoading: !this.state.isLoading,
            }, ()=> {
                axios.get(fullURL)
                .then((response) => {
                    // console.warn(response.data);
                    this.setState({
                        locationName: "",
                        isLoading: false,
                    }, ()=> {
                        this.props.navigation.navigate('DetailsListScreen', {url1: url1, url2: url2, page: page, responseData: response.data});
                    });
                })
                .catch((error) => {
                    this.setState({
                        locationName: "",
                        isLoading: false,
                    }, () => {
                        this.generateAlert(error.message);
                    })
                    return false;

                })
            })
        }
    }

    toggleButtonAndLoader = () => {
        if(this.state.isLoading){
            return(
                <View style={{width: '20%', height: 40, justifyContent: 'center', alignItems: 'center', }}>
                    <ActivityIndicator size="large" color="rgb(0, 136, 209)" />
                </View>
            )
        } else{
            return(
                <TouchableOpacity style={{width: '20%', height: 40, justifyContent: 'center', alignItems: 'center', }} onPress={()=> this.callRestApi()}>
                    <Text style={{fontSize: 20, fontWeight: 'bold', color: 'rgb(0, 139, 214)'}}>GO</Text>
                </TouchableOpacity>
            )
        }
    }

  render() {
    return (
      <View style={{flex: 1,}}>

        {/* Code for the body part of the screen */}
        <View style={{flex: 1, alignItems: 'center', }}>
            <Text style={{fontSize: 15, fontWeight: 'bold', marginTop: 10,}}>Search for houses to buy</Text>
            <Text style={{fontSize: 15, fontWeight: 'bold', marginTop: 10, }}>Search by place name or postcode.</Text>
            <View style={{flexDirection: 'row', height: 80, marginTop: 10, justifyContent: 'center', alignItems: 'center', }}>
                <View style={{width: '75%', height: 40, borderWidth: 1, borderColor: 'rgb(0, 136, 212)', borderRadius: 10, justifyContent: 'center', marginLeft: 5, }}>
                    <TextInput 
                    style={{fontSize: 18, padding: 10, color: 'rgb(0, 136, 212)'}}
                    placeholder="Location OR Pincode"
                    onChangeText={(text) => this.setState({ locationName: text })}
                    value={this.state.locationName}
                    editable={!this.state.isLoading}
                    />
                </View>
                {
                    this.toggleButtonAndLoader()
                }
            </View>
            <View style={{width: '100%', height: 200, justifyContent: 'center', alignItems: 'center',}}> 
                <Image source={require('./images/home.png')} style={{width: 100, height: 100}}/>
            </View>
        </View>
      </View>
    );
  }
}
