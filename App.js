//Importing react navigation.
import { createStackNavigator, createSwitchNavigator } from "react-navigation";

// Importing screens.
import HomeScreen from './src/homeScreen.js';
import DetailsListScreen from './src/detailsListScreen.js';
import DetailsViewScreen from './src/detailsViewScreen.js';


//configuration for app stack.
const AppStack = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Property Finder',
      }
    },
    DetailsListScreen: {
      screen: DetailsListScreen,
      navigationOptions: {
        title: 'Results',
      }
    },
    DetailsViewScreen: {
      screen: DetailsViewScreen,
      navigationOptions: {
        title: 'Property',
      }
    }
  },
  {
    initialRouteName: "HomeScreen"
  }
);

export default createSwitchNavigator(
  {
    App: AppStack,
  },
  {
    initialRouteName: "App"
  }
);
